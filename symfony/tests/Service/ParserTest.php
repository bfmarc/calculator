<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\Parser;

/**
 * Testing Parser service
 */
class ParserTest extends KernelTestCase
{
    /** @test */
    public function parse(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $parser = $container->get(Parser::class);
        $parser->parse('2+2*3/4');
        $this->assertEquals(['2','+','2','*','3','/','4'], $parser->getData());
    }
}
