<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\Calculator;

/**
 * Testing Calculator service
 */
class CalculatorTest extends KernelTestCase
{
    /** @test */
    public function calculate(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $calculator = $container->get(Calculator::class);
        $calculator->calculate('2+2*3/3');
        $this->assertEquals(4, $calculator->getResult());
    }
}
