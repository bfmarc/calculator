<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\Parser;
use App\Service\Calculator;

/**
 * Command to calculate the formula
 */
class Calculate extends Command
{
    private $calculator;

    public function __construct(Calculator $calculator)
    {
        parent::__construct();
        $this->calculator = $calculator;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('calculatrice:calculate')
            ->setDescription('Evaluate a formula.')
            ->addArgument(
                'formula',
                InputArgument::REQUIRED,
                'The formula to calculate'
            );
    }

    /**
     * {@inheritdoc}
     * @return int
     * @throws Exception if the elements are not numeric
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $formula = $input->getArgument('formula');

        $this->calculator->calculate($formula);

        $output->writeln(sprintf('The result is : <comment>%s</comment>', $this->calculator->getResult()));

        return Command::SUCCESS;
    }
}
