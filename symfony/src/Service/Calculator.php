<?php

namespace App\Service;
use App\Kernel;

/**
 * class to calculate the formula
 */
class Calculator
{
  /**
   * @var total
   */
  private $total;
  /**
   * @var operators
   */
  private $operators;
  /**
   * @var container
   */
   private $container;

  /**
   * Constructor
   *
   * @param $operators
   * @param Container $container
   */
  public function __construct($operators, Kernel $kernel)
  {
      $this->total = 0;
      $this->operators = $operators;
      $this->container = $kernel->getContainer();
  }

  /**
   * Constructor
   *
   * @param string $formula
   *
   * @throws Exception if the elements are not numeric
   */
  public function calculate($formula): void
  {
    $parser = new Parser($this->operators);
    $parser->parse($formula);
    $data = $parser->getData();
    foreach($this->operators as $key => $operatorService) {
        while (in_array($key, $data)) {
          for ($i = 0; $i < count($data); $i++) {
              if ($data[$i] == $key) {
                  $data[$i] =  $this->container->get($operatorService)->getValue($data[$i-1], $data[$i+1]);
                  array_splice($data, $i-1, 1);
                  array_splice($data, $i, 1);
                  break;
              }
          }
        }
    }
    $this->total = $data[0];
	}

  /**
   * get the result
   *
   * @return
   */
  public function getResult()
  {
    return $this->total;
  }
}
