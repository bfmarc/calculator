<?php

namespace App\Service;

use \Exception as Exception;

/**
 * class to parse the string formula in an array of tokens
 */
class Parser
{
  /**
   * @var data
   */
  private $data;
  /**
   * @var operators
   */
  private $operators;

  /**
   * Constructor
   *
   * @param $operators
   */
  public function __construct($operators)
  {
      $this->data = [];
      $this->operators = $operators;
  }

  /**
   * parse the formula
   *
   * @param string $formula
   *
   * @throws Exception if the elements are not numeric
   */
  public function parse(String $formula): void
  {
    $formula = str_replace(".", "dot", $formula);
    $numbers = preg_split( '/[+*-\/]/', $formula,-1, PREG_SPLIT_OFFSET_CAPTURE );
    $this->data = [];
    for ($i = 0; $i < count($numbers); $i++) {
        $number = str_replace("dot", ".", $numbers[$i][0]);
        if (!is_numeric($number)) {
          throw new Exception('Elements must be numbers.');
        }
        $this->data[] =  (float) $number;
        $operator = substr($formula, $numbers[$i][1] + strlen($numbers[$i][0]), 1);
        if (! empty($operator)) $this->data[] =  $operator;
    }
	}

  /**
   * get the data
   *
   * @return array
   */
	public function getData(): array
  {
    return $this->data;
	}
}
