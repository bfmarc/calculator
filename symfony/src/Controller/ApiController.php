<?php

namespace App\Controller;

use App\Service\Calculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * API controller
 */
class ApiController extends AbstractController
{
    /**
     * endpoint to calculate the formula
     *
     * @Route("/api/calculate", methods={"POST"}, name="api_calculate")
     *
     * @param Request $request
     * @param Calculator $calculator
     *
     * @throws Exception if the elements are not numeric
     */
    public function index(Request $request, Calculator $calculator): Response
    {
        $calculator->calculate($request->get('formula'));
        $response = new Response(json_encode(['result' => $calculator->getResult()]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
