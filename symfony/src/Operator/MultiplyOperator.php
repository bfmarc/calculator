<?php

namespace App\Operator;

/**
 * Calculate the multiply operation
 */
class MultiplyOperator implements IOperator {

    public function getValue(float $op1, float $op2) {
        return $op1 * $op2;
    }
}
