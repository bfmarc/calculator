<?php

namespace App\Operator;

/**
 * Calculate the minus operation
 */
class MinusOperator implements IOperator {

    public function getValue(float $op1, float $op2) {
        return $op1 - $op2;
    }
}
