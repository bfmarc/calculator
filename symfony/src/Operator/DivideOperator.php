<?php

namespace App\Operator;

/**
 * Calculate the divide operation
 */
class DivideOperator implements IOperator {

    public function getValue(float $op1, float $op2) {
        return $op1 / $op2;
    }
}
