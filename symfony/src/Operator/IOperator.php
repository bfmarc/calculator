<?php

namespace App\Operator;

interface IOperator {
    public function getValue(float $op1, float $op2);
}
